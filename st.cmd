require essioc
require vac_ctrl_digitelqpce,1.5.0

iocshLoad("${essioc_DIR}/common_config.iocsh")

epicsEnvSet("DEVICE_NAME", "Spk-030LWU:Vac-VEPI-10001")
epicsEnvSet("MOXA_HOSTNAME", "moxa-vac-spk-20-u006.tn.esss.lu.se")
epicsEnvSet("MOXA_PORT", "4001")

iocshLoad("${vac_ctrl_digitelqpce_DIR}/vac_ctrl_digitelqpce_moxa.iocsh", "DEVICENAME = $(DEVICE_NAME), IPADDR = $(MOXA_HOSTNAME), PORT = $(MOXA_PORT)")

iocshLoad("${vac_ctrl_digitelqpce_DIR}/vac_pump_digitelqpce_vpi.iocsh", "DEVICENAME = Spk-030LWU:Vac-VPN-10000, CHANNEL = 1, CONTROLLERNAME = $(DEVICE_NAME)")
iocshLoad("${vac_ctrl_digitelqpce_DIR}/vac_pump_digitelqpce_vpi.iocsh", "DEVICENAME = Spk-040LWU:Vac-VPN-10000, CHANNEL = 2, CONTROLLERNAME = $(DEVICE_NAME)")
